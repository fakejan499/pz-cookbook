import {db} from "../../src/shared/db";

describe('pz-cookbook', () => {
  beforeEach(async () => {
    await db.delete();
  })

  it('Add and remove favourite recipe', () => {
    cy.visit('/search-result/Breakfast')
    const addToFavouriteButton = cy.get('[data-cy="addToFavourite"]').first();
    addToFavouriteButton.click();
    cy.visit('/favourites');
    const favourites = cy.get('[data-cy="favouriteRecipeCard"]');
    favourites.should('have.length', 1);
    favourites.get('[data-cy="addToFavourite"]').first().click();
    favourites.should('have.length', 0);
  })

  it('Last viewed recipes', function () {
    cy.visit('/');
    cy.get('[data-cy="last-viewed-recipe"]').should('have.length', 0);

    cy.visit('/search-result/Breakfast');
    const recipeCard = cy.get('[data-cy="search-result-recipe"]').first();
    recipeCard.click();
    cy.wait(3000); // time for loading recipe details and add it to DB

    cy.visit('/');
    cy.get('[data-cy="last-viewed-recipe"]').should('have.length', 1);
  });

  it('Search bar', function () {
    const searchPhrase = 'gluten free';
    cy.visit('/');
    cy.get('[data-cy="search-bar-open-btn"]').first().click();
    cy.get('[data-cy="search-input"]').first().type(searchPhrase);
    cy.get('[data-cy="search-submit"]').first().click();

    cy.url().should("contain", searchPhrase.replace(' ', '%20'));
    cy.get('[data-cy="search-result-title"]').first().should("contain", searchPhrase);
  });
})
