import Hello from "./Hello.svelte";
import RateRecipe from "./rate-recipe/RateRecipe.svelte";
import LastViewed from "./last-viewed/LastViewed.svelte";

// @ts-ignore
const comp = new Hello({ });
// @ts-ignore
const rateRecipe = new RateRecipe({ });
// @ts-ignore
const lastViewed = new LastViewed({ });

export default {comp, rateRecipe, lastViewed};
