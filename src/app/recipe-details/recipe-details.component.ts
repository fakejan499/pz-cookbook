import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {RecipeService} from "../services/recipe.service";
import {db} from "../../shared/db";
import {map, shareReplay, startWith, switchMap, tap} from "rxjs";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  servingsInput = new FormControl(1, [Validators.min(1)]);
  recipe$ = this.recipeService.getRecipeDetails(+(this.route.snapshot.paramMap.get('id') ?? 1)).pipe(
    tap(recipe => db.lastViewed.put({...recipe, timestamp: Date.now()})),
    tap(recipe => this.servingsInput.setValue(recipe.servings)),
    shareReplay()
  );
  ingredients$ = this.recipe$.pipe(
    switchMap(recipe => this.servingsInput.valueChanges.pipe(
      startWith(this.servingsInput.value),
      map(servings => recipe.ingredients.map(ing => ({...ing, amount: (ing.amount * (servings / recipe.servings)).toFixed(1)})))
    ))
  )

  constructor(private readonly recipeService: RecipeService,
              private readonly route: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
