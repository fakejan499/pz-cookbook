import { ComponentsModule } from './../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecipeDetailsRoutingModule } from './recipe-details-routing.module';
import { RecipeDetailsComponent } from './recipe-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [RecipeDetailsComponent],
  imports: [
    CommonModule,
    RecipeDetailsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
  ],
})
export class RecipeDetailsModule {}
