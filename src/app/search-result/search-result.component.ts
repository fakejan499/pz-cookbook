import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {RecipeService} from "../services/recipe.service";
import {map, switchMap} from "rxjs";

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
  readonly recipes$ = this.route.paramMap.pipe(
    map(p => p.get('category') || ''),
    switchMap(category => this.recipeService.getRecipes(category))
  );
  readonly title$ = this.route.paramMap.pipe(map(p => p.get('category')));

  constructor(private readonly route: ActivatedRoute,
              private readonly recipeService: RecipeService) { }

  ngOnInit(): void {
  }

}
