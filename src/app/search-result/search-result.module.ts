import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchResultRoutingModule } from './search-result-routing.module';
import {SearchResultComponent} from './search-result.component';
import {ComponentsModule} from "../components/components.module";


@NgModule({
  declarations: [
    SearchResultComponent
  ],
  imports: [
    CommonModule,
    SearchResultRoutingModule,
    ComponentsModule
  ]
})
export class SearchResultModule { }
