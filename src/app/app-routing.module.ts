import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesComponent } from './recipes/recipes.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: RecipesComponent },
  {
    path: 'recipe',
    loadChildren: () =>
      import('./recipe-details/recipe-details.module').then(
        (m) => m.RecipeDetailsModule
      ),
  },
  {
    path: 'search-result',
    loadChildren: () =>
      import('./search-result/search-result.module').then(
        (m) => m.SearchResultModule
      ),
  },
  {
    path: 'favourites',
    loadChildren: () =>
      import('./favourites/favourites.module').then((m) => m.FavouritesModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
