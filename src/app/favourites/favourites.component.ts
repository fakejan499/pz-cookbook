import { Component, OnInit } from '@angular/core';
import { liveQuery } from 'dexie';
import { from } from 'rxjs';
import { db } from 'src/shared/db';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css'],
})
export class FavouritesComponent implements OnInit {
  favourites$ = liveQuery(() => db.favourite.toArray());

  constructor() {}

  ngOnInit(): void {}
}
