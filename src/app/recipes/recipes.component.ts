import {Component} from '@angular/core';
import {CategoryService} from "../services/category.service";
import {liveQuery} from "dexie";
import {db} from "../../shared/db";

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent {
  categories$ = this.categoryService.getCategories();


  constructor(private readonly categoryService: CategoryService) {
  }

}
