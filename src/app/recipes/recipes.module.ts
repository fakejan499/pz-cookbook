import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecipesComponent } from './recipes.component';
import {ComponentsModule} from "../components/components.module";
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [
    RecipesComponent
  ],
    imports: [
        CommonModule,
        ComponentsModule,
        RouterModule,
    ],
  exports: [RecipesComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecipesModule { }
