import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ComponentsModule } from './components/components.module';
import {HttpClientModule} from "@angular/common/http";
import {RecipesModule} from "./recipes/recipes.module";

@NgModule({
  declarations: [AppComponent, HeaderComponent, FooterComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [BrowserModule, AppRoutingModule, FormsModule, ComponentsModule, HttpClientModule, RecipesModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
