import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Category} from "../../shared/models/category.model";
import {forkJoin, map, Observable} from "rxjs";
import {Recipe} from "../../shared/models/recipe.model";
import {Ingredient} from "../../shared/models/ingredient.model";

@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  private apiKey = environment.apiKey;
  private apiURL = environment.apiURL;

  constructor(private http: HttpClient) {}

  getRecipes(query?: string, category?: Category): Observable<Recipe[]> {
    let params = new HttpParams().set('apiKey', this.apiKey).set('number', 50);
    if (query) params = params.set('query', query);
    if (category) params = params.set(category.type, category.title);

    return this.http
      .get(`${this.apiURL}/recipes/complexSearch`, { params })
      .pipe(
        map((result: any) =>
          result.results.map((recipe: any) => {
            return {
              id: recipe.id,
              title: recipe.title,
              image: recipe.image,
              ingredients: [],
              preparationSteps: [],
            };
          })
        )
      );
  }

  getRecipeDetails(id: number): Observable<Recipe> {
    return forkJoin([
      this.getRecipeInfo(id),
      this.getRecipePreparationSteps(id),
      this.getRecipeIngredients(id),
    ]).pipe(
      map(([recipeInfo, recipePreparationSteps, recipeIngredients]) => {
        return {
          ...recipeInfo,
          preparationSteps: recipePreparationSteps,
          ingredients: recipeIngredients,
        };
      })
    );
  }

  private getRecipeInfo(id: number): Observable<Recipe> {
    let params = new HttpParams().set('apiKey', this.apiKey);

    return this.http
      .get(`${this.apiURL}/recipes/${id}/information`, { params })
      .pipe(
        map((recipe: any) => {
          return {
            id: recipe.id,
            title: recipe.title,
            image: recipe.image,
            ingredients: [],
            preparationSteps: [],
            servings: recipe.servings
          };
        })
      );
  }

  private getRecipePreparationSteps(id: number): Observable<string[]> {
    let params = new HttpParams().set('apiKey', this.apiKey);

    return this.http
      .get(`${this.apiURL}/recipes/${id}/analyzedInstructions`, { params })
      .pipe(
        map((response: any) => {
          return response[0].steps.map((step: any) => step.step);
        })
      );
  }

  private getRecipeIngredients(id: number): Observable<Ingredient[]> {
    let params = new HttpParams().set('apiKey', this.apiKey);

    return this.http
      .get(`${this.apiURL}/recipes/${id}/ingredientWidget.json`, { params })
      .pipe(
        map((response: any) =>
          response.ingredients.map((ingredient: any) => {
            return {
              name: ingredient.name,
              amount: ingredient.amount.metric.value,
              unit: ingredient.amount.metric.unit,
            };
          })
        )
      );
  }
}
