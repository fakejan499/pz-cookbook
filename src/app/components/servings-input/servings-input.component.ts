import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-servings-input',
  templateUrl: './servings-input.component.html',
  styleUrls: ['./servings-input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ServingsInputComponent),
      multi: true,
    },
  ],
})
export class ServingsInputComponent implements ControlValueAccessor {
  @Input() name: string = '';
  value = 0;

  constructor() {}

  increase() {
    this.value++;
    this.onChange(this.value);
  }

  decrease() {
    this.value--;
    this.onChange(this.value);
  }

  onChange: any = () => {};
  onTouched: any = () => {};

  writeValue(val: number): void {
    this.value = val;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
