import { Component, OnInit } from '@angular/core';
import {liveQuery} from "dexie";
import {db} from "../../../shared/db";

@Component({
  selector: 'app-last-viewed',
  templateUrl: './last-viewed.component.html',
  styleUrls: ['./last-viewed.component.css']
})
export class LastViewedComponent implements OnInit {
  readonly lastViewed$ = liveQuery(() => db.lastViewed.toCollection().reverse().sortBy('timestamp'));

  constructor() { }

  ngOnInit(): void {
  }

}
