import {Recipe} from './../../../shared/models/recipe.model';
import {Component, Input} from '@angular/core';
import {liveQuery} from "dexie";
import {db} from "../../../shared/db";

@Component({
  selector: 'app-add-to-favourites-button',
  templateUrl: './add-to-favourites-button.component.html',
  styleUrls: ['./add-to-favourites-button.component.css'],
})
export class AddToFavouritesButtonComponent {
  @Input() recipe: Recipe | null = null;

  readonly isFavourite$ = liveQuery(async () => {
    if (!this.recipe) return false;
    const r = await db.favourite.where(':id').equals(this.recipe.id).first();
    return !!r;
  });

  toggleFavourite() {
    const sub = this.isFavourite$.subscribe(isFavourite => {
      sub.unsubscribe();
      if (!this.recipe) return;

      if (isFavourite) db.favourite.delete(this.recipe.id);
      else db.favourite.put(this.recipe!);
    })
  }
}
