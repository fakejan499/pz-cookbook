import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToFavouritesButtonComponent } from './add-to-favourites-button.component';

describe('AddToFavouritesButtonComponent', () => {
  let component: AddToFavouritesButtonComponent;
  let fixture: ComponentFixture<AddToFavouritesButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddToFavouritesButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToFavouritesButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
