import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Recipe } from '../../../shared/models/recipe.model';
import {liveQuery} from "dexie";
import {db} from "../../../shared/db";

@Component({
  selector: 'app-recipe-card',
  templateUrl: './recipe-card.component.html',
  styleUrls: ['./recipe-card.component.css'],
})
export class RecipeCardComponent implements OnInit {
  @Input() recipe: Recipe | null = null;

  readonly isFavourite$ = liveQuery(async () => {
    if (!this.recipe) return false;
    const r = await db.favourite.where(':id').equals(this.recipe.id).first();
    return !!r;
  });

  constructor(private router: Router) {}

  ngOnInit(): void {}

  showRecipeDetails() {
    this.router.navigate(['recipe', this.recipe?.id]);
  }

  toggleFavourite() {
    const sub = this.isFavourite$.subscribe(isFavourite => {
      sub.unsubscribe();
      if (!this.recipe) return;

      if (isFavourite) db.favourite.delete(this.recipe.id);
      else db.favourite.put(this.recipe!);
    })
  }
}
