import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Category } from 'src/shared/models/category.model';

@Component({
  selector: 'app-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.css'],
})
export class CategoryCardComponent implements OnInit {
  @Input() category: Category | null = null;

  @HostBinding('style.background-image') background: string = '';

  constructor() {}

  ngOnInit(): void {
    this.background = `
      linear-gradient(hsla(0, 0%, 50%, 0.5), hsla(0, 0%, 50%, 0.5)),
      url('${this.category?.photo}')
    `;
  }
}
