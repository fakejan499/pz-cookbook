import { Router } from '@angular/router';
import { RecipeService } from './../../services/recipe.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-of-the-day',
  templateUrl: './recipe-of-the-day.component.html',
  styleUrls: ['./recipe-of-the-day.component.css'],
})
export class RecipeOfTheDayComponent implements OnInit {
  private recipeID = this.resetTime(new Date()).getTime() % 1000000;
  recipe$ = this.recipeService.getRecipeDetails(this.recipeID);

  constructor(private recipeService: RecipeService, private router: Router) {}

  ngOnInit(): void {}

  private resetTime(date: Date): Date {
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);

    return date;
  }

  showRecipeDetails() {
    this.router.navigate(['recipe', this.recipeID]);
  }
}
