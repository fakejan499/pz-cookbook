import {Component, ElementRef, EventEmitter, HostListener, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent {
  @ViewChild('form') formRef!: ElementRef<HTMLFormElement>;
  @Output() search = new EventEmitter();
  isModalOpen = false;
  query = '';

  constructor() {
  }

  showSearchModal(e: Event) {
    e.stopPropagation()
    this.isModalOpen = true;
  }

  @HostListener('window:click', ['$event'])
  onWindowClick(event: MouseEvent): void {
    if (this.isModalOpen && !this.formRef.nativeElement.contains((event.target as HTMLElement))) {
      this.isModalOpen = false;
    }
  }

  searchFor(query: string) {
    this.search.emit(query);
    this.isModalOpen = false;
    this.query = '';
  }

}
