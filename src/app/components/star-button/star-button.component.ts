import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-star-button',
  templateUrl: './star-button.component.html',
  styleUrls: ['./star-button.component.css']
})
export class StarButtonComponent implements OnInit {
  @Input() fill = false;

  constructor() { }

  ngOnInit(): void {
  }

}
