import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryCardComponent } from './category-card/category-card.component';
import { RouterModule } from '@angular/router';
import { RecipeCardComponent } from './recipe-card/recipe-card.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { FormsModule } from '@angular/forms';
import { RecipeOfTheDayComponent } from './recipe-of-the-day/recipe-of-the-day.component';
import { AddToFavouritesButtonComponent } from './add-to-favourites-button/add-to-favourites-button.component';
import { ServingsInputComponent } from './servings-input/servings-input.component';
import { StarButtonComponent } from './star-button/star-button.component';
import { LastViewedComponent } from './last-viewed/last-viewed.component';

@NgModule({
  declarations: [
    CategoryCardComponent,
    RecipeCardComponent,
    SearchBarComponent,
    RecipeOfTheDayComponent,
    AddToFavouritesButtonComponent,
    ServingsInputComponent,
    StarButtonComponent,
    LastViewedComponent,
  ],
  imports: [CommonModule, RouterModule, FormsModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [
        CategoryCardComponent,
        RecipeCardComponent,
        SearchBarComponent,
        RecipeOfTheDayComponent,
        AddToFavouritesButtonComponent,
        ServingsInputComponent,
        LastViewedComponent,
    ],
})
export class ComponentsModule {}
