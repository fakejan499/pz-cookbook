import Dexie, {Table} from "dexie";
import {Recipe} from "./models/recipe.model";
import {LastViewedRecipe} from "./models/last-viewed-recipe.model";

class CookbookDBImp extends Dexie {
  favourite!: Table<Recipe>;
  lastViewed!: Table<LastViewedRecipe>;

  constructor() {
    super('pz-cookbook');

    this.version(1).stores({
      favourite: '&id',
      lastViewed: '&id, timestamp'
    })
  }
}

export type CookbookDB = CookbookDBImp;
export const db = new CookbookDBImp();
