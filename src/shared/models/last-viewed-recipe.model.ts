import {Recipe} from "./recipe.model";

export interface LastViewedRecipe extends Recipe {
  timestamp: number;
}
