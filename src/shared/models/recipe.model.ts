import { Ingredient } from './ingredient.model';

export interface Recipe {
  id: number;
  title: string;
  image: string;
  ingredients: Ingredient[];
  preparationSteps: string[];
  servings: number;
}
