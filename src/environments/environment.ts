// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiURL: 'https://api.spoonacular.com/',
  apiKey: 'feb9269f4a8b40b18970a130582d6099',
  // apiKey: '86728ef8247046cdb57dfad3a9795097',
  // apiKey: '821f8bcd661246cfb7901beaaec17cb9',
  // apiKey: '110368c717ad41048aabf18a4d89eb4f',
  storageIndexPrefix: '997',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
